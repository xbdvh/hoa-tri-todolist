$(document).ready(function() {
    $('.date').datepicker();
    $('.date.tomorrow').on('changeDate', function(ev) {
        var today = new Date;
        today.setHours(0);
        today.setMinutes(0);
        today.setSeconds(0);
        today.setMilliseconds(0);
        var tomorrow = new Date(today.getTime() + 24 * 3600 * 1000);
        if (ev.date.valueOf() < tomorrow.valueOf())
        {
            $(this).find('[type="text"]').val("");
        }
    });

    $('#add-keyword').click(function() {
        var select = $('#keywords :selected');
        if (select) {
            var value = $('[name=keyword_ids]').val();
            $('[name=keyword_ids]').val(value + ',' + select.val());

            var value1 = $('[name=seo_keywords]').val()
            $('[name=seo_keywords]').val(select.html() + ',' + value1);

            select.remove();
        }
    });
});