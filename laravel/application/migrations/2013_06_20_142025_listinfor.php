<?php

class Listinfor {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::table('listinfor', function($table)
            {
                $table->create();
                $table->increments('id');
                $table->string('email',100);
                $table->string('title');
                $table->integer('priority');
                $table->date('startTime');
                $table->date('duration');
                $table->string('description');
               
            });
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}