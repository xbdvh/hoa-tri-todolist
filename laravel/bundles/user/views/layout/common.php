<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MangoTools</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

	<?php echo Asset::container('bootstrapper')->styles(); ?>
	<?php echo Asset::container('header')->styles(); ?>
	<?php echo Asset::container('header')->scripts(); ?>

    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- This code is taken from http://twitter.github.com/bootstrap/examples/hero.html -->

	<section id="topbar">
	</section>
	<section>
	    <?php echo Section::yield('content') ?>
	</section>
        <section>

	    <?php echo Asset::container('bootstrapper')->scripts(); ?>
	    <?php echo Asset::container('footer')->scripts(); ?>
	    <?php echo Section::yield('scripts') ?>

	</section>
    </body>
</html>
