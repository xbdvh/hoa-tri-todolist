<html>
<head>
    <title>login</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css"/>
    <script src="js/bootstrap.js"></script>
</head>
<body>
    <form name="myForm" class="well" method="POST" action="user/checklogin">
        <fieldset>
            <legend>Sign in </legend>
                <label>Email: </label>
                <input type="text" class="span3" placeholder="Type username here" name="dn_email"/>
                
                <label>Password: </label>
                <input type="password" class="span3" placeholder="Type password here" name="dn_password"/>
                <br />
                <button class="btn btn-primary">Login</button>
                 <a href="index" class="btn">Reset</a>
                 <br />
                 <br />
                 if you didn't have account,click  <a href="user/signup/signup">here</a>
        </fieldset>
    </form>
</body>
</html>