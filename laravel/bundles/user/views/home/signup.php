<?php Section::start('content');
?>
<html>
<head>
    <title>login</title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css"/>
    <script src="js/bootstrap.js"></script>
    <script language="JavaScript">
    function checkinput(){
        email = document.formsignup.email;
        pass = document.formsignup.password;
        repass = document.formsignup.repassword;
        reg1=/^[0-9A-Za-z]+[0-9A-Za-z_]*@[\w\d.]+.\w{2,4}$/;
        testmail = reg1.test(email.value);
         if(!testmail){
            alert("Email is invalid,Email with format:abc@xyz.ef");
            email.focus();
            return false;
        }
        if((pass.value == "")||(pass.value.length < 6))
        {
            alert("Password at least 6 character");
            pass.focus();
            return false;
        }
        
        if(pass.value != repass.value)
        {
            alert("Not match between password and repassword");
            repass.focus();
            return false;
        }
        }
     </script>
</head>
<body>
    <form  class="well" method="post" action="http://laravel.com/user/insertdata/insertdata" name="formsignup" onsubmit="return checkinput();">
        <fieldset>
            <legend>Sign up </legend>
                <label>Email: </label> <input type="text" class="span3" placeholder="Email" name="email"/>
                
                <label>Password: </label> <input type="password" class="span3" placeholder="Password" name="password"/>
                <label>Repeat Password: </label> <input type="password" class="span3" placeholder="Repeat Password" name="repassword"/>
                
                <br />
                <br />
                    <button class="btn ">Sign up</button>
                    <a href="http://laravel.com" class="btn">Back</a>
                  
                <br />
        </fieldset>
    </form>
</body>
</html>
<?php Section::stop() ?>