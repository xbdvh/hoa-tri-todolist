<?php
Asset::container('header')
	->bundle('user')
	->add('mondernizr-js', 'js/modernizr-2.6.2-respond-1.1.0.min.js')
	->add('datepicker_css', 'datepicker/css/datepicker.css')
	->add('main_css', 'css/main.css');
Asset::container('footer')
	->bundle('user')
	->add('datepicker_js', 'datepicker/js/bootstrap-datepicker.js')
	->add('main_js', 'js/main.js');
