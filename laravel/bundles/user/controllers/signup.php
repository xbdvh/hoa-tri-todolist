<?php

class User_Signup_Controller extends Base_Controller {

    public $layout = "user::layout.common";

    public function __construct() {
        parent::__construct();
      //  Helper::redirect_url(URL::full());
    }

    function action_signup() {
        $data = array();
     $this->layout->nest('content', 'user::home.signup', $data);
     
    }

}

?>