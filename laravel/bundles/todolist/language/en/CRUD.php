<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    "add new project" => "Add new Project",
    'name' => 'Name',
    'seo_main_site' => 'Main site',
    'seo_search_engine' => 'Search engine', 
    'seo_language' => 'Language',
    'seo_competitors' => 'Competitors',
    'seo_start_date' => "Start date",
    'seo_duration' => 'Duration',
    'view all %s projects' => 'View all :total Projects',
    'option actions' => 'Actions',
    'page view title %s' => 'View :name',
    'page title add' => 'Add new Project',
    'submit title add' => 'Create',
    'submit title update' => 'Update',
    'page title edit %s' => "Edit :name",
    'submit title update' => "Update",
    'seo_keywords' => 'Keywords',
    'keywords comma split' => 'split by comma',
    
    );
?>
