<?php

class Todolist_List_Controller extends Base_Controller {

    public $layout = "todolist::layout.common";

    public function __construct() {
        parent::__construct();
        Helper::redirect_url(URL::full());
    }

    function action_index() {
        $data = array();
        $this->layout->nest('content', 'todolist::list.index', $data);
    }

}

?>